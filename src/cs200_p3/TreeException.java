package cs200_p3;

public class TreeException extends RuntimeException{
	private static final long serialVersionUID = 1L;
	TreeException(String errorText)
	{
		super(errorText);
	}
}
