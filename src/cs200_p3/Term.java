package cs200_p3;
import java.util.LinkedList;

/**
 * 
 * @author Daniel Ball
 *
 */
public class Term implements Comparable<Term>{
	private String name;	//KEY VALUE
	private int docFrequency;	
	private int totalFrequency;

	LinkedList<Occurrence> docList = new LinkedList<Occurrence>();
	
	
	/**
	 * @return
	 * Number of documents in which word appears
	 */
	public int getDocFrequency()
	{
		return docFrequency;
	}

	/**
	 * @return
	 * total number of occurrences of word in all documents
	 */
	public int getTotalFrequency()
	{
		return totalFrequency;
	}
	
	 //stores the word (name) and initializes the docFrequency to 0
	/**Instantiate term with left and right terms null**/
	public Term(String name)
	{
		this.name = name.toLowerCase();
		docFrequency = 0;
		totalFrequency = 0;
	}
	
	public int compareNum(Term other)
	{
		return (this.totalFrequency - other.totalFrequency);
	}
	
	/** increments totalFrequency and either <br>
	 * a) creates a new Occurrence object if there is not one with document as its docName 
	 * and increments docFrequency<br>
	 * b) increments the frequency of Occurrence with document as its docName.
	 */
	public void incFrequency(String document)
	{
		totalFrequency++;
		
		Occurrence temp = new Occurrence(document);
		//Case: Occurrence exists w/ docName in docList
		//increments the frequency of Occurrence
		if(docList.contains(temp))
		{
			(docList.get(docList.indexOf(temp))).incFrequency();
		}
		//Case: Occurrence DNE w/ docName in docList
		//creates a new Occurrence object 
		//increments docFrequency
		else
		{
			docFrequency++;
			docList.add(temp);
		}
	}
	
	/**
	 * Compares compares name members of two terms, or name member of a term to an input string.
	 * @param
	 * 		testTerm
	 * 		::Type must be String or Term for comparison 
	 * @return
	 * ::Case: param is a String<br>
	 * -True if Term.name is lexicographically equal to param<br>
	 * -False otherwise<br>
	 * ::Case: param is a Term<br>
	 * -True if Term.name is lexicographically equal to param.name<br>
	 * -False otherwise<br>
	 * ::Case: param is neither<br>
	 * -false
	 */
	@Override
	public boolean equals(Object testTerm)
	{
		if(testTerm instanceof Term)
		{
			if(this.name.equals(((Term)testTerm).name))
			{
				return true;
			}
		}
		else if(testTerm instanceof String)
		{
			if(this.name.equals(((String) testTerm).toLowerCase()))
			{
				return true;
			}
		}
		return false;
	}	
	@Override
	public String toString()
	{
		return (name);
	}

	public String getName()
	{
		return name;
	}
	@Override
	public int compareTo(Term other) 
	{
		return this.name.compareTo(other.name.toLowerCase());
	}
	
	public int compareName(String word)
	{
		return this.name.compareTo(word.toLowerCase());
	}
}
