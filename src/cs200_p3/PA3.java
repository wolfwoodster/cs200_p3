package cs200_p3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class PA3
{
	public static void main(String[] args)
	{

		WebPages searchEngine = new WebPages(); // WebPages object for current "search engine"
		String currentInput = null; // holds current input from inStream
		boolean fileNamesStop = false; // true if file still taking in page names
		ArrayList<String> wordChecker = new ArrayList<String>(); // Contains words to check with whichPages();

		// Add pages and read words to check
		try
		{
			Scanner inStream = new Scanner(new File(args[0])); // file containing input filenames and directives

			while (fileNamesStop == false)
			{
				if (!inStream.hasNext())
				{
					System.err.println("Problem reading input file: does not contain *EOFs*");
					System.exit(0);
				}
				currentInput = inStream.next();
				if (currentInput.contentEquals("*EOFs*"))
				{
					fileNamesStop = true;
				} else
				{
					inStream.nextLine();
					searchEngine.addPage(currentInput);
				}
			}
			while (inStream.hasNext())
			{
				String wordsToCheck = inStream.next();
				wordChecker.add(wordsToCheck);
			}
			inStream.close();

		} catch (FileNotFoundException e)
		{
			System.err.println("File not found!");
			System.exit(0);
		} catch (IndexOutOfBoundsException e)
		{
			System.err.println("Missing argument for input file");
			System.exit(0);
		}
		// Print words (same format as PA2)
		searchEngine.printTerms();

		// Run whichPages on Webpages object for each word in input file
		for (int i = 0; i < wordChecker.size(); i++)
		{

			String[] temp = searchEngine.whichPages(wordChecker.get(i));
			if (temp == null)
			{
				System.out.println(wordChecker.get(i) + " not found");
			} else
			{
				System.out.print(wordChecker.get(i) + " in pages: ");
				for (int j = 0; j < temp.length; j++)
				{
					System.out.print(temp[j]);
					if (j != temp.length - 1)
					{
						System.out.print(", ");
					}
				}
				System.out.println();
			}
		}

		// Print level where word is found(performed by BST get, part of whichPages())
		// if found, print "word in pages: page1, page2..." (see output example)
		// If not found, print "[word] not found"
	}
}
