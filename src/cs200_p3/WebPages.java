package cs200_p3;

//-------------------------------------------------------------------------------------------------
//mergeSortAlpha() and mergeSortNum() are implementations from
//Data Abstraction and Problem Solving with Java, 3rd edition, 2011, Frank Carrano, Janet Prichard.
//pp 529-531.
//-------------------------------------------------------------------------------------------------

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * @author Daniel Ball <daniel.s.ball@gmail.com>
 * @version 3
 * @since The Dawn of Time
 */
// Organizes term objects in BST of terms.
public class WebPages
{
	private BST termIndex;
	int pageCount = 0;

	// initializes a new index of Terms
	public WebPages()
	{
		termIndex = new BST();
	}

	/**
	 * parse file referenced by filename into individual words adds those words
	 * and their counts to the termIndex.
	 */
	public void addPage(String filename)
	{
		// <.*?>match a set of closed html brackets with unlimited content as a
		// delimiter
		Pattern tagPattern = Pattern.compile("<.*?>");

		// "[^a-zA-z0-9']" sets delimiter as any character NOT listed.
		Pattern wordPattern = Pattern.compile("[^a-zA-z0-9']+");

		// String containing file stripped of HTML tags
		String strippedString = "";

		String tempWord = "";

		try
		{
			Scanner inStream = new Scanner(new File(filename));
			inStream.useDelimiter(tagPattern);
			/* Strip code segments from file and create new temporary string */
			if (!inStream.hasNext())
			{
				System.err.println("Error: Empty File");
			}
			while (inStream.hasNext())
			{
				strippedString += (inStream.next() + "");
			}
			inStream.close();

			/* Parse strippedString for words. Add each to termIndex. */
			Scanner cleanScan = new Scanner(strippedString);
			cleanScan.useDelimiter(wordPattern);
			while (cleanScan.hasNext())
			{
				tempWord = cleanScan.next().toLowerCase();

				// add to termIndex BST
				termIndex.add(filename, tempWord);
			}
			cleanScan.close();
			pageCount++;

		} catch (IOException e)
		{
			System.err.println("Error: " + e);
			System.err.println("Input file inaccessible.");
		} catch (ArrayIndexOutOfBoundsException e)
		{
			System.err.println("Error: " + e);
			System.err.println("No input arguments were supplied.");
		}
	}

	// print terms, 1 line per term
	public void printTerms()
	{
		BSTIterator termIterator = new BSTIterator(termIndex);
		termIterator.setInorder();
		System.out.println("WORDS");
		while (termIterator.hasNext())
		{
			System.out.println(termIterator.next().toString());
		}
		System.out.println("");

	}

	/**
	 * Search for word in termIndex.
	 * 
	 * @param word
	 *            word to find
	 * @return -array with document names from Term for "word" followed by TFIDF(magnitude, 2 decimal places)<br>
	 *         -null if not found
	 */
	public String[] whichPages(String word)
	{
		DecimalFormat DF = new DecimalFormat("0.00");	//Format to 
		Term foundTerm = termIndex.get(word, true);
		if(foundTerm == null)
		{
			return null;
		}
		if (foundTerm.equals(word))
		{
			int index = 0;
			int size = foundTerm.docList.size();
			String[] temp = new String[size];
			for (Occurrence doc : foundTerm.docList)
			{
				
				temp[index] = doc.getName() + ": " ;
				if(pageCount != 0 && size != 0)
				{
					temp[index] += DF.format(doc.calcTFIDF(pageCount, size ));
				}
				else
				{
					temp[index] += "TFIDF undefined";
				}
							
				index++;
			}
			return temp;
		} else
		{
			return null; 
		}
	}

	
	
	/**
	 * concept from "Data Abstraction and Problem Solving," Prichard, 611
	 * modified to use BST
	 * @author Daniel Ball
	 *
	 * @param <T>
	 */
	public class BSTIterator implements Iterator<Term> {
		private BST binTree;
		private TreeNode<Term> currentNode;
		private LinkedList <TreeNode<Term>> queue;
		
		public BSTIterator(BST termIndex)
		{
			binTree = termIndex;
			currentNode = null;
			queue = new LinkedList < TreeNode<Term>>();		
		}
		@Override
		public void remove()
		{
			throw new UnsupportedOperationException();
		}
		
		@Override
		public boolean hasNext() {
			return !queue.isEmpty();
		}

		@Override
		public Term next() throws java.util.NoSuchElementException {
			currentNode = queue.remove();
			return currentNode.payload;
		}
		
		public void setPreorder(){
			queue.clear();
			preorder(binTree.root);
		}
		
		public void setInorder(){
			queue.clear();
			inorder(binTree.root);
		}
		
		public void setPostorder(){
			queue.clear();
			postorder(binTree.root);
		}
		
		private void preorder(TreeNode<Term> treeNode){
			if(treeNode != null){
				queue.add(treeNode);
				preorder(treeNode.leftChild);
				preorder(treeNode.rightChild);
			}
		}
		
		private void inorder(TreeNode<Term> treeNode){
			if(treeNode != null){
				inorder(treeNode.leftChild);
				queue.add(treeNode);
				inorder(treeNode.rightChild);
			}
		}
		
		private void postorder(TreeNode<Term> treeNode){
			if(treeNode != null){
				postorder(treeNode.leftChild);
				postorder(treeNode.rightChild);
				queue.add(treeNode);
			}
		}
	}
	
	/**
	 * Class implements binary search tree for WebPages
	 * 
	 * @author Daniel Ball
	 *
	 */
	private class BST extends BinaryTreeBasis<Term>
	{
		int count; // number of populated nodes

		/** initialize root, count **/
		public BST()
		{
			super();
			count = 0;
		}

		/**
		 * 
		 * @param word
		 * @param b
		 * @return
		 */
		public Term get(String word, boolean b)
		{
			TreeNode<Term> temp = findClosest(word, b);
			if (temp.payload.equals(word))
			{
				return temp.payload;
			} else
			{
				return null;
			}
		}

		/** returns number of unique words in document. **/
		public int size()
		{
			return count;
		}

		/**
		 * @param newItem
		 *            Root payload
		 */
		@Override
		public void setRootItem(Term newItem)
		{
			if (this.isEmpty())
			{
				throw new TreeException("TreeException: Root is null.");
			} else
			{
				root.payload = newItem;
			}

		}

		/**
		 * Add a new Term or increment frequency
		 * **/
		public void add(String documentName, String word)
		{
			int temp;
			TreeNode<Term> searchNode = findClosest(word, false);
			if (searchNode == null) // Tree is empty, add to root
			{
				root = new TreeNode<Term>(new Term(word), null, null);
				root.payload.incFrequency(documentName);
				return;
			}

			TreeNode<Term> foundNode = findClosest(word, false);
			temp = -foundNode.payload.compareName(word); // negative switches
															// from comparing
															// foundNode key to
															// word, to
															// comparing word to
															// foundNode key
			if (temp == 0)
			{
				foundNode.payload.incFrequency(documentName); // If payload key
																// matches word,
																// increment
																// frequency
			} else if (temp < 0) // If word precedes payload key, set to left
									// child, increment frequency
			{
				foundNode.leftChild = new TreeNode<Term>(new Term(word), null,
						null);
				foundNode.leftChild.payload.incFrequency(documentName);
			} else
			// If word follows payload key, set to left child, increment
			// frequency
			{
				foundNode.rightChild = new TreeNode<Term>(new Term(word), null,
						null);
				foundNode.rightChild.payload.incFrequency(documentName);
			}

		}

		/**
		 * 
		 * @param word
		 *            key value to find
		 * @return TreeNode containing term, TreeNode to which term would be
		 *         attached, or null if empty tree.
		 */
		private TreeNode<Term> findClosest(String word, boolean printDepth)
		{
			TreeNode<Term> current = root;
			int depth = 1;
			int test;

			if (current == null)
			{
				return null; // Return null if empty
			}
			while (current != null)
			{
				test = -current.payload.compareName(word); 	// comparing current to search term, so negative compares search term to current
				if (test == 0) 								// Case: current Term matches key value, end traversal
				{
					if (printDepth)
					{
						System.out.println("  At depth " + depth);
					}
					return current;
				} // case: search term precedes current term
				else if (test < 0)
				{
					if (current.leftChild == null)
					{
						if(printDepth)
						{
							depth++;
							System.out.println("  At depth " + depth);
						}
						return current;
					}
					current = current.leftChild;
				} else
				// case: search term follows current term
				{
					if (current.rightChild == null)
					{
						if(printDepth)
						{
							depth++;
							System.out.println("  At depth " + depth);
						}
						return current;
					}
					current = current.rightChild;
				}
				depth++; // Increment depth and continue search
			}
			throw new TreeException("TreeException: Error locating term");
		}
	}
}