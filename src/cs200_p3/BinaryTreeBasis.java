package cs200_p3;

/* concept from "Data Abstraction and Problem Solving," Prichard, 605 */
public abstract class BinaryTreeBasis<T> {
	protected TreeNode<T> root;
	
	/**initialize root**/
	public BinaryTreeBasis()
	{
		root = null;
	}
	
	public BinaryTreeBasis(TreeNode<T> rootNode)
	{
		root = rootNode;
	}
	
	public boolean isEmpty()
	{
		return root == null;
	}
	
	public void makeEmpty()
	{
		root = null;
	}
	
	public T getRootItem() throws TreeException
	{
		if(root == null )
		{
			throw new TreeException("TreeException: Empty Tree");
		}
		else
		{
			return root.payload;
		}
	}
	
	public abstract void setRootItem(T newItem);
}
