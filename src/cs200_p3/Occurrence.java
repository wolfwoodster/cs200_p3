package cs200_p3;

public class Occurrence
{
	private String docName; // Stores name of document
	private int termFrequency; // number of times term has occurred

	// stores the docName and initializes the termFrequency to 1.
	public Occurrence(String name)
	{
		docName = name;
		termFrequency = 1;
	}

	// Increments term frequency
	public void incFrequency()
	{
		++termFrequency;
	}

	/**
	 * Term Frequency - Inverse Document Frequency<br>
	 * Provides a double indicating a weighted relevance to search term<br>
	 * TFIDF = termFrequency*log(totalDocs/docFrequency)
	 * 
	 * 
	 * @param termFrequency
	 *            number of occurrences of term in document
	 * @param totalDocs
	 *            total number of available documents
	 * @param docFrequency
	 *            number of documents containing term
	 * @return calculated TFIDF
	 */
	public double calcTFIDF(int totalDocs, int docFrequency)
	{
		// cast to doubles to prevent truncation
		double TF = termFrequency;
		double D = totalDocs;
		double DF = docFrequency;
		// calculate and return
		return Math.abs(TF * Math.log(D / DF));
	}

	// @Override
	public boolean equals(Object testName)
	{
		if (testName instanceof Occurrence)
		{
			if (this.docName.equals(((Occurrence) testName).docName))
			{
				return true;
			} else
			{
				return false;
			}
		}
		return false;
	}

	// @Override
	public String toString()
	{
		return "DOCUMENT: " + docName + " | FREQUENCY IN DOCUMENT: " + termFrequency;
	}

	public String getName()
	{
		return docName;
	}
}